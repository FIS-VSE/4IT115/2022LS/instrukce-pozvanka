# Instrukce pro přijetí pozvánky na Gitlab

Pro bezproblémové přijetí pozvánky do školního repozitáře zaslané na Váš školní email se držte následujících instrukcí:

## 1. V pozvánce klikněte na Join

![Pozvánka](01-invitation.png)

## 2. Vyplňte registrační údaje klikněte na Register. Použijte **školní email**, na který pozvánka přišla (měl by být předvyplněný).

![Otevřená pozvánka s registračním formulářem](02-register.png)

## 3. Zvolte libovolné personalizační údaje a zakliknětě "Get started".

![Uvítání s personalizací](03-welcome.png)

## 4. Po potvrzení se zobrazí v horní části obrazovky informace, že jste se stali členem skupiny s názvem, který odpovídá Vašemu xname.

![Informace o členství ve skupině](04-joined.png)

## 5. Po kliknutí na Váš xname nebo ikonu skupiny v levém horním rohu se dostanete na úvodní stránku skupiny, kde můžete zakládat své projekty.

![Úvodní stránka skupiny](05-group.png)

## Existující účet

Pozvánku by mělo jít přijmout i po přihlášení do Vašeho existujícího účtu (namísto registrace). **Před přijetím pozvánky přidejte školní email** mezi emaily, které se váží k Vašemu existujícímu GitLab účtu.

## Neposílat žádosti o přístup

V žádném případě neposílejte žádosti o vstup do některé ze školních skupin. Vaše osobní skupina není viditelná a do jiné skupiny Vám přístup zamítneme.
